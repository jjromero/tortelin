from google.cloud import dataproc_v1
from google.api_core.client_options import ClientOptions

##MAIN_PYTHON_FILE_URI será el archivo /processor.processor.py del proyecto que será subido en cloud storage
MAIN_PYTHON_FILE_URI = 'gs://bigdata_tortelin_bucket/processor.py'
PARENT = 'projects/big-data-360519/locations/us-central1'
ENDPOINT = 'us-central1-dataproc.googleapis.com:443'
CONTAINER_IMAGE = 'gcr.io/big-data-360519/con-google-cloud:1.0'

def init_next_step():
    # Create a client
    options = ClientOptions(api_endpoint=ENDPOINT)
    client = dataproc_v1.BatchControllerClient(client_options=options)

    # Initialize request argument(s)
    runtime_config = dataproc_v1.RuntimeConfig(container_image=CONTAINER_IMAGE)
    batch = dataproc_v1.Batch(runtime_config=runtime_config)

    #batch = dataproc_v1.Batch()
    batch.pyspark_batch.main_python_file_uri = MAIN_PYTHON_FILE_URI

    request = dataproc_v1.CreateBatchRequest(
        parent=PARENT,
        batch=batch,
    )

    # Make the request
    client.create_batch(request=request)
