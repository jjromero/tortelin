import os
from shared.auth import authenticate_runtime
from shared.secrets import getSecret
from scraper import scrape
from nextStep import init_next_step

# Autenticar usuario
authenticate_runtime()

# Configurar entorno
PROJECT_ID = '292410231981'
os.environ['COOKIE_C_USER'] = getSecret(PROJECT_ID, 'COOKIE_C_USER', 'latest')
os.environ['COOKIE_XS'] = getSecret(PROJECT_ID, 'COOKIE_XS', 'latest')
os.environ['SCRAPE_YEARS_AGO'] = getSecret(PROJECT_ID, 'SCRAPE_YEARS_AGO', 'latest')
os.environ['TORTELIN_PAGE_ID'] = getSecret(PROJECT_ID, 'TORTELIN_PAGE_ID', 'latest')
os.environ['BUCKET_NAME'] = getSecret(PROJECT_ID, 'BUCKET_NAME', 'latest')
os.environ['BUCKET_FILE_NAME'] = getSecret(PROJECT_ID, 'BUCKET_FILE_NAME', 'latest')

# Iniciar scraping
scrape()

# Iniciar siguiente etapa
init_next_step()

# Apagar al terminar
MODE = getSecret(PROJECT_ID, 'SCRAPER_MODE', 'latest')
if MODE == 'AUTO':
    os.system('sudo shutdown -h now')
