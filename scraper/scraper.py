import json
from facebook_scraper import get_posts
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from shared.storage import upload_text
import os

def scrape():
    cookies = {
        "c_user": os.environ['COOKIE_C_USER'],
        "xs": os.environ['COOKIE_XS'],
    }

    options = {
        "comments": True
    }

    farthest_date = datetime.now() - relativedelta(years=int(os.environ['SCRAPE_YEARS_AGO']))
    posts = get_posts(
        os.environ['TORTELIN_PAGE_ID'],
        pages=99999,
        cookies=cookies,
        options=options
    )

    allPosts = []
    for post in posts:
        if post['time'] < farthest_date:
            break
        allPosts.append(post)

    BLOB_STR = json.dumps(allPosts, sort_keys=True, default=str) 
    today = date.today()
    DATE_STR = ('{}_{}_{}').format(today.year, today.month, today.day)

    # ARCHIVO FINAL
    upload_text(
        os.environ['BUCKET_NAME'],
        os.environ['BUCKET_FILE_NAME'] + '.json',
        BLOB_STR
    )

    # ARCHIVO HISTORIAL
    upload_text(
        os.environ['BUCKET_NAME'],
        os.environ['BUCKET_FILE_NAME'] + '_' + DATE_STR + '.json',
        BLOB_STR
    )
