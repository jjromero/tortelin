# TORTELIN
## INGESTA
Para realizar la ingesta de los datos se está utilizando web scraping a la página de facebook de tortelin.
La configuración que se deb realizar es la siguiente:

### 1. INICIO
1. Ingresar a una cuenta de google cloud y crear un proyecto
2. Descargar de google cloud IAM el archivo credentials.json del usuario que e utilizará para el proyecto y conservarlo
3. Crear un bucket para almacenar toda la información del proyecto
4. Crear una cuenta en send grid y crear un api key
5. Configurar en la consola de Google Secret Manager los siguientes secretos
- **BUCKET_NAME** = Nombre del bucket creado (ejm: bucket_tortelin)
- **BUCKET_FILE_NAME** = Nombre que tendrá el archivo resultado del scraping (ejm: posts). Solo nombre, sin extensión
- **COOKIE_C_USER** = Cookie C_USER de un usuario con sesión iniciada en facebook (ejm:dfxbecvt4gce4vtnyhyvrm-tdb)
- **COOKIE_XS** = Cookie XS del mismo usuario con sesión iniciada en facebook (ejm: 8745yuthrj9847uirehjt)
- **SCRAPE_YEARS_AGO** = La cantidad en años que se tomará en cuenta para la extracción (ejm: 5)
- **SCRAPER_MODE** = AUTO si la instancia se deberá apagar al terminar el scraping y el llamado al procesamiento (ejm: AUTO)
- **SENDGRID_API_KEY** = El API KEY extraido de send grid para el envío de correo (ejm: i76ybhnji8u7ygtvbhnj)
- **TORTELIN_NOTIFICACION_FROM_EMAIL** = El correo dsde el cual se enviara la notificación (adasd@example.com)
- **TORTELIN_NOTIFICACION_TEMPLATE_ID** = La plantilla de del correo de sendgrid que se utilizará para la notificación (ejm: 765rtyui87654)
- **TORTELIN_NOTIFICACION_TO_EMAIL** = El correo al cual se le notificará (ejm: adasd@example.com)
- **TORTELIN_PAGE_ID** = El id de la página de tortelin (aunque a estas alturas puede ser de cualquier página pública de facebook) (ejm: Tortelin.PasteleriaCreativa)

### 2. SCRAPING
Realizar los siguientes pasos para configurar la instancia de scraping
1. Crear una instancia de compute engine que utilice una imagen de debian
2. Configurar el programador de instancia para encender la instancia recién creada el día que se desee iniciar el procesamiento
3. Ingresar por ssh a la instancia encendida
4. Clonar el repositorio en el home del usuario en uso _/home/usuario/_ (se creará una carpeta nueva _/home/usuario/**tortelin**/_)
5. Copiar el archivo del repositorio **extra/tortelin-scraper.service** a _/etc/systemd/system/_ en la instancia cambiando el usuario y el user para que sea el que esté configurado en el servidor
6. Agregar el contenido del archivo del repositorio **extra/visudo.entry** al final del archivo de la instancia _/etc/sudoers_
7. Copiar el archivo del repositorio **init.sh.example** con el nombre **init.sh** y configurar el nombre del usuario
8. Copiar el archivo **credentials.json** dentro del directorio _/home/usuario/tortelin/shared/_
9. Ejecutar el siguiente comando para configurar el servicio al inicio de la instancia
```sh
sudo systemctl enable tortelin-scraper.service
```
## 3. PROCESAMIENTO
Realizar los siguientes pasos para crear la imagen que se utilizará en el procesamiento por batch de los datos recogidos
1. Abrir un terminal de google (google-shell)
2. Crear un archivo llamado **Dockerfile** y copiar en su interior lo que contiene el archivo del repositorio **Dockerfile.dataproc-serverless**
3. Copiar el archivo **credentials.json** en el directorio actual de la consola
4. Ejecutar en la terminal los comandos contenido en el archivo del repositorio **cloud-shell-commands.md** configurando el id del proyecto

