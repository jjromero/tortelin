from google.cloud import storage

def upload_text(bucket_name, file_name, text):
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(file_name)

    blob.upload_from_string(text)

def download_text(bucket_name, file_name):
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(file_name)

    return blob.download_as_text()
