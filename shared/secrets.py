from google.cloud import secretmanager_v1

def getSecret(projectId, name, version):
    # Create a client
    client = secretmanager_v1.SecretManagerServiceClient()

    # Initialize request argument(s)
    request = secretmanager_v1.AccessSecretVersionRequest(
        name=('projects/{}/secrets/{}/versions/{}').format(projectId, name, version),
    )

    # Make the request
    response = client.access_secret_version(request=request)

    # Handle the response
    return response.payload.data.decode('UTF-8')
