import os

def authenticate_runtime():
    CREDENTIALS_PATH = os.environ['SCRAPER_BASE_PATH'] + '/shared/credentials.json'
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = CREDENTIALS_PATH