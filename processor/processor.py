#!pip install --upgrade google-cloud-language
#!pip install facebook-scraper
#!pip install pandas sklearn matplotlib
#!pip install google-cloud-secret-manager
#!pip install google-cloud-storage

import numpy as np
from google.cloud import storage
from google.cloud import secretmanager_v1
import os
import pandas as pd
from io import StringIO
import matplotlib.pyplot as plt
from sendgrid.helpers.mail import Mail
from sendgrid import SendGridAPIClient

## Configuración del entorno 
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/spark/credentials.json'
PROJECT_ID = '292410231981'

## Helpers
def getSecret(projectId, name, version):
  client = secretmanager_v1.SecretManagerServiceClient()
  request = secretmanager_v1.AccessSecretVersionRequest(
      name=('projects/{}/secrets/{}/versions/{}').format(projectId, name, version),
  )
  response = client.access_secret_version(request=request)
  return response.payload.data.decode('UTF-8')

def download_text(bucket_name, file_name):
  client = storage.Client()
  bucket = client.get_bucket(bucket_name)
  blob = bucket.blob(file_name)
  return blob.download_as_text()

  
def upload_text(bucket_name, file_name, text):
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(file_name)

    blob.upload_from_string(text)

def SendDynamic(FROM_EMAIL, TO_EMAIL, TEMPLATE_ID):
    message = Mail(
        from_email=FROM_EMAIL,
        to_emails=[TO_EMAIL]
    )
    message.template_id = TEMPLATE_ID
    sg = SendGridAPIClient(getSecret(PROJECT_ID, 'SENDGRID_API_KEY', 'latest'))
    response = sg.send(message)
    return str(response.status_code)
####

################# Carga de datos
BLOB_STR = download_text(
    getSecret(PROJECT_ID,  'BUCKET_NAME', 'latest'),
    getSecret(PROJECT_ID, 'BUCKET_FILE_NAME', 'latest') + '.json'
)
df_raw = pd.read_json(StringIO(BLOB_STR))
#df_raw.count()

## Primera seleccion de datos usables
df = df_raw[['post_id', 'text', 'post_url', 'comments', 'comments_full', 'reactions', 'reaction_count', 'shares', 'timestamp']]
#df.head()
#df.describe()
#df.isnull().sum()
#df.isna().sum()
#df.dtypes

################# Preparacion de los datos

### Agregar cantidad de interacciones (score)
df = df.assign(score=df['comments'] + df['reaction_count'] + df['shares'])
df.head()

### Agregar horas (hour)
def getHour(datetime):
  return datetime.hour

df = df.assign(hour=df['timestamp'].transform(getHour))
df.head()

### Mostrar datos
#hours = np.arange(0,24,1)
#df[['score', 'hour']].plot.scatter(x='hour', y='score', alpha=0.3, figsize=(10, 14), xticks=hours, s=100, c='#4cc25b')

### Limpiar datos extremos
q_hi  = df["score"].quantile(0.95)
df_filtered = df[ (df["score"] < q_hi) ]

### Mostrar datos
#print( "Eliminados:  " + str(df.shape[0] - df_filtered.shape[0]))
#df_filtered[['score', 'hour']].plot.scatter(x='hour', y='score', alpha=0.05, figsize=(10, 14), xticks=hours, s=100, c='#4cc25b')

################# Procesamiento
df_usable = df_filtered[['hour', 'score']]
df_usable = df_usable.assign(quantity=1)
total_per_hour = df_usable.groupby('hour').count()
total_records = len(df_usable.index)

df_final = pd.DataFrame()
for hour in total_per_hour.index:
  new_rows = pd.DataFrame()
  new_rows = new_rows.append(df_usable[df_usable['hour'] == hour].groupby('score', as_index=False).agg({'hour': 'max', 'quantity': 'count'}))
  new_rows = new_rows.assign(total_in_hour=total_per_hour.at[hour, 'quantity'])
  new_rows = new_rows.assign(probability=new_rows['quantity'] / new_rows['total_in_hour'])
  new_rows = new_rows.assign(reliability=new_rows['total_in_hour'] / total_records)
  new_rows = new_rows.sort_values(by=['probability'], ascending=False)

  #df_final = df_final.append(new_rows)
  df_final = df_final.append(new_rows.iloc[:3])

#print(df_final.groupby('hour', as_index=False).sum())
#print(df_final.groupby('hour', as_index=False).max().sum())
#df_final.head(20)
#df_final[df_final['hour'] == 18]
#total_per_hour

################# Guardado de resultados
df_sorted = df_final.sort_values(by=['probability', 'reliability'], ascending=[False, False])
df_sorted.reset_index(inplace=True)
df_sorted.head(10)
df_sorted = df_sorted[df_sorted['reliability'] > 0.01]

upload_text(
  getSecret(PROJECT_ID,  'BUCKET_NAME', 'latest'),
  'tortelin_processed.csv',
  df_sorted.to_csv()
)

#df_sorted.head()
sizes=df_sorted['reliability']*3000
df_sorted.plot.scatter(x='score', y='probability', alpha=0.3, figsize=(20, 10), s=sizes, c='#4cc25b')

SendDynamic(
    FROM_EMAIL=getSecret(PROJECT_ID, 'TORTELIN_NOTIFICACION_FROM_EMAIL', 'latest'),
    TO_EMAIL=getSecret(PROJECT_ID, 'TORTELIN_NOTIFICACION_TO_EMAIL', 'latest'),
    TEMPLATE_ID=getSecret(PROJECT_ID, 'TORTELIN_NOTIFICACION_TEMPLATE_ID', 'latest'),
)